-- CREATE USER IF NOT EXISTS myuser;
-- CREATE DATABASE IF NOT EXISTS myappdb;
-- GRANT ALL PRIVILEGES ON DATABASE myappdb TO myuser;

CREATE TABLE IF NOT EXISTS "students" (
    "id" varchar(50),
	"name" varchar (50) NOT NULL,
	count int NOT NULL,
	PRIMARY KEY("id", "name")
);