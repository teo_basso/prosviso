import React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect, actions } from '../store';

const requiredError = 'This field is required';

const styles = {
  wrapper: {
    width: '100vw',
    height: '100vh',
    background: '#607D8B',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    padding: 24,
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: 8
  },
  error: {
    marginTop: 16,
    color: '#FF0000'
  }
}

class Page extends React.Component {

  state = {
    id: null,
    studentName: null
  }

  isFormValid = () => {
    const { studentName, id } = this.state;
    return studentName && id;
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  onKeyDown = event => {
    if ((event.which || event.keyCode) === 13 && this.isFormValid()) {
      this.onSubmit();
    }
  };

  onSubmit = () => {
    const { studentName, id } = this.state;

    actions.enter({
      name: studentName,
      id
    });
  }

  render() {
    const { loading, error, student } = this.props;
    const { studentName, id } = this.state;

    const isIdRequired = id !== null && !id;
    const isStudentNameRequired = studentName !== null && !studentName;

    return (
      <div style={styles.wrapper}>
        <Paper style={styles.paper}>
          {loading ? (
            <CircularProgress />
          ) : (
            <>
              <Typography variant="h5" component="h3">
                {student ?  (
                  `Hello ${student.name}, your id is ${student.id} and you visited this page ${student.count} times`
                ) : (
                  'Insert name and id'
                )}
              </Typography>
              {!student && (
                <>
                  <TextField
                    label="Student name"
                    value={studentName !== null ? studentName : ''}
                    onChange={this.handleChange('studentName')}
                    onKeyDown={this.onKeyDown}
                    error={isStudentNameRequired}
                    helperText={isStudentNameRequired && requiredError}
                    margin="normal"
                    required
                  />
                  <TextField
                    label="Id"
                    value={id !== null ? id : ''}
                    onChange={this.handleChange('id')}
                    onKeyDown={this.onKeyDown}
                    error={isIdRequired}
                    helperText={isIdRequired && requiredError}
                    margin="normal"
                    required
                  />
                  <Button
                    variant="contained"
                    color="primary"
                    style={styles.button}
                    onClick={this.onSubmit}
                    disabled={!this.isFormValid()}
                  >
                    Submit
                  </Button>
                  {error && (
                    <Typography style={styles.error}>
                      Something went wrong, please try again
                    </Typography>
                  )}
                </>
              )}
            </>
          )}
        </Paper>
      </div>
    );
  }
}

export default connect(({ data, ...others }) => ({ student: data, ...others }))(Page);
