import React from 'react';
import axios from 'axios';
import * as enzyme from 'enzyme';
import toJson from 'enzyme-to-json';
import MockAdapter from 'axios-mock-adapter';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

enzyme.configure({ adapter: new Adapter() });

describe('App', () => {
  let mock;

  beforeEach(() => {
    mock = new MockAdapter(axios);
  });
  
  afterEach(() => {
    mock.restore();
    mock.reset();
  });
  
  it('should render a form', () => {
    const tree = enzyme.shallow(<App />);
    expect(toJson(tree)).toMatchSnapshot();
  });
  
  it('should render a loader', (done) => {
    mock.onPost('/api/student').reply(() => new Promise(resolve => {
      setTimeout(() => {
        resolve([400]);
      }, 2000);
    }));

    const component = enzyme.mount(<App />);
    const usernameInput = component.find('input').at(0);
    usernameInput.instance().value = 'foo';
    usernameInput.simulate('change');
    const idInput = component.find('input').at(1);
    idInput.instance().value = 'bar';
    idInput.simulate('change');
    component.find('button').at(0).simulate('click');
    setTimeout(() => {
      component.update();
      expect(toJson(component)).toMatchSnapshot();
      setTimeout(() => done(), 2000);
    }, 1000);
  });
  
  it('should render an error', (done) => {
    mock.onPost('/api/student').reply(400);

    const component = enzyme.mount(<App />);
    const usernameInput = component.find('input').at(0);
    usernameInput.instance().value = 'foo';
    usernameInput.simulate('change');
    const idInput = component.find('input').at(1);
    idInput.instance().value = 'bar';
    idInput.simulate('change');
    component.find('button').at(0).simulate('click');
    setTimeout(() => {
      component.update();
      expect(toJson(component)).toMatchSnapshot();
      done();
    }, 1000);
  });

  it('should render a student', (done) => {
    mock.onPost('/api/student').reply(config => {
      const data = JSON.parse(config.data);
      return [200, {
        ...data,
        count: 7
      }]
    });

    const component = enzyme.mount(<App />);
    const usernameInput = component.find('input').at(0);
    usernameInput.instance().value = 'foo';
    usernameInput.simulate('change');
    const idInput = component.find('input').at(1);
    idInput.instance().value = 'bar';
    idInput.simulate('change');
    const submitButton = component.find('button').at(0);
    submitButton.instance().click();
    submitButton.simulate('click');
    setTimeout(() => {
      component.update();
      expect(toJson(component)).toMatchSnapshot();
      done();
    }, 1000);
  });
});
