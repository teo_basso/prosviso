import axios from 'axios';
import createStore from 'react-waterfall';

const getStudent = axios.post.bind(null, '/api/student');

const config = {
  initialState: {},
  actionsCreators: {
    enter: async (_, actions, student, trigger) => {
        if (!trigger) await actions.enter(student, true);
        else return { loading: true, error: false };

        try {
          const { data } = await getStudent(student);
          return {
            loading: false,
            error: false,
            data
          };
        } catch (ex) {
          return {
            loading: false,
            error: true
          };
        }
    },
  },
}

export const { Provider, connect, actions } = createStore(config);
