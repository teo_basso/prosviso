const request = require('supertest');
const chai = require('chai');
const app = require('./'); //reference to you app.js file


describe('Test connection', () => {
    it('respond at "GET /" with an hello', (done) => {
        request(app)
            .get('/')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect('"hello there, API are working!"')
            .expect(200, done);
    });
});


describe('Test "POST /student"', () => {
    it('should get 500 with empty request body', (done) => {
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(500, done);
    });
    it('should get 500 with missing id', (done) => {
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .send({ "name": "matteo" })
            .expect('Content-Type', /json/)
            .expect(500)
            .expect('{"msg":"Missing name and/or id."}', done);
    });
    it('should get 500 with missing name', (done) => {
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .send({ "id": "0" })
            .expect('Content-Type', /json/)
            .expect(500)
            .expect('{"msg":"Missing name and/or id."}', done);
    });
    it('should get 200 with valid request body', (done) => {
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .send({ "name": "matteo", "id": "0" })
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
    it('should create new user', (done) => {
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .send({ "name": "123456789adasad0", "id": "0" })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, response) => {
                chai.assert(response.body.count == 1, `New users count must be 1.`);
                done();
            });
    });
    it('should increment response.count after 2 sequential identical and valid requests', (done) => {
        let firstCount = -1, secondCount = -1;
        request(app)
            .post('/student')
            .set('Accept', 'application/json')
            .send({ "name": "matteo", "id": "0" })
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, response) => {
                firstCount = response.body.count;
                request(app)
                    .post('/student')
                    .set('Accept', 'application/json')
                    .send({ "name": "matteo", "id": "0" })
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end((err, response) => {
                        secondCount = response.body.count;
                        chai.assert(firstCount + 1 == secondCount,
                            `Sequential identical requests fail on response.count increment (firstCount: ${firstCount}, secondCount: ${secondCount}).`);
                        done();
                    });
            }, done);
    });
});