const express = require('express');
const bodyParser = require('body-parser');
const { Pool, Client } = require('pg');
const pool = new Pool();
const app = express();

const query = (...args) => new Promise((resolve, reject) => {
    const params = [...args, (err, res) => {
        if (err) reject(err);
        else resolve(res);
    }];
    pool.query(...params);
});

app.use(bodyParser.json());

app.get('/', (_, res) => res.json('hello there, API are working!'));

const updateCount = (id, name, rows) => {
    let count;
    let msg;
    let promise;
    if (rows.length) { // increment
        count = rows[0].count + 1;
        msg = 'Cannot increment your login count.';
        promise = query(`UPDATE "students" SET "count"=$1 WHERE "id"=$2 AND "name"=$3`, [count, String(id), name]);
    } else { // creation
        count = 1;
        msg = 'Cannot create new user.';
        promise = query(`INSERT INTO "students" ("id", "name", count) VALUES ($1, $2, 1)`, [String(id), name]);
    }

    return promise
        .then(() => ({ id, name, count }))
        .catch(err => Promise.reject({ msg, err }));
};

app.post('/student', (request, response) => {
    if (request.body.id && request.body.name)
        query(`SELECT * FROM "students" WHERE "id"=$1 AND "name"=$2`, [String(request.body.id), request.body.name])
            .then(res => {
                updateCount(request.body.id, request.body.name, res.rows)
                    .then(result => { response.json(result); })
                    .catch(error => { response.status(500).send(error); });
            })
            .catch(err => {
                response.status(500).send({ msg: 'Db connection failed.', err });
            });
    else
        response.status(500).send({ msg: 'Missing name and/or id.' });
});


module.exports = app.listen(8888, () => {
    console.log('Example app listening on port 8888!');
});
